public class Jellyfish {
    
    public boolean deadly;
    public double bodySize;
    public String region;
    
    public Jellyfish(boolean deadly, int bodySize, String region) {
        this.deadly = deadly;
        this.bodySize = bodySize;
        this.region = region;
    }
    
    public boolean isDeadly() {
        if (deadly) {
            return true;
        } else {
            return false;
        }
    }
    
    public String regionFound() {
        return "This jellyfish is located in " + region;
    }
}
